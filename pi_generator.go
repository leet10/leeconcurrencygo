// pi_generator.go
// Calculates the value of pi using a very inefficient stochastic method, and determine if this can be sped up by use of concurrency.
// Author: Tou Ko Lee

package main

import (
	"fmt"
	"math/rand"
	"math"
	"time"
	"sync"
)


// Calculates the value of pi using a very inefficient stochastic method
// throws is the number of darts wanted to be thrown
// returns pi which is float64
func estimatePi(throws int) float64{
	var dartsThrown float64 = 0
	var dartsIn float64 = 0
	var throwsFloat float64 = float64(throws)

	// keeps track of the number of darts thrown and which are in the circle
	for (throwsFloat > dartsThrown) {
		dartsThrown++

		var x float64 = rand.Float64()
		var y float64 = rand.Float64()
		// checks if the darts are within the circle
		if (x * x + y * y <= 1.0){
			dartsIn++
		}
	}
	// calculates pi from only throwing darts in 1/4 of the unit circle
	var pi float64 = 4 * dartsIn / dartsThrown
	return pi
}

// Gets the number of throws inside the 1/4 of the circle
// throws is the number of darts wanted to be thrown
func run(throws int, channel chan int){
	var dartsThrown int = 0
	var dartsIn int = 0

	// keeps track of the number of darts thrown and which are in the circle
	for (throws > dartsThrown) {
		dartsThrown++

		var x float64 = rand.Float64()
		var y float64 = rand.Float64()
		// checks if the darts are within the circle
		if (x * x + y * y <= 1.0){
			dartsIn++
		}
	}
	channel <- dartsIn
}

// Calculates pi
// in is the number of throws inside the circle
// throws is the number of throws taken
// returns pi which is float64
func proprotion(in, throws int) float64{
	return 4 * float64(in) / float64(throws)
}

// Automates throws with concurrency
func conExperiment(throws int) {
	threads := 10
	var samples = make(chan int, 100)

	now := time.Now()
	for i:= 0; i < threads; i++ {
		go run(throws, samples)
	}
	var propTotal float64 = 0
	for i:= 0; i < threads; i++ {
		propTotal += float64(<- samples)
	} 
	propTotal = proprotion(int(propTotal), threads * throws)
	elapsed := time.Since(now)
	delta := propTotal - math.Pi

	fmt.Printf("Throws taken:%-12d Estimated pi:%-14f delta:%+-14.14f Time taken:%v\n", threads * throws, propTotal, delta, elapsed.Seconds())
}

// Automates an experiment from 10k throws to 10g
func experiment() {
	pi := math.Pi
	for i := 0; i <= 3; i++ {
		var throws int = 10000 * int(math.Pow10(i))
		start := time.Now()
		estPi := estimatePi(throws)
		elapsed := time.Since(start)
		delta := estPi - pi
		fmt.Printf("Throws taken:%-12d Estimated pi:%-14f delta:%+-14.14f Time taken:%v\n", throws, estPi, delta, elapsed.Seconds())
	}
}



// driver function
func main() {
	var wg sync.WaitGroup
    calcPi := func( index int) {
        defer wg.Done()
        conExperiment(10000 * int(math.Pow10(index)))
    }
	for i := 3; i >= 0; i-- {
		wg.Add(1)
		go calcPi(i)
	}
	wg.Wait()
}