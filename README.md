﻿Summary:
A program using the Go programming language to calculate the value of pi using a very inefficient stochastic method (throwing darts in a one by one unit on a graph and multiples it by 4 to get calculate pi), and determine if this can be sped up by use of concurrency.

Contents/files and folders:
The program will consist of only one file called "pi_generator.go" where it contains the neccesary methods needed to generate pi from the number of darts wanted to be thrown.

How to use project:
Using the command line, go to the directory where "pi_generator.go" is and run it by typing "go run filename", where filename is the name of the file (pi_generator.go).

